# Tailscale


This Ansible role installs and configures the [Tailscale client](https://tailscale.com/download)
on Ubuntu Jammy computers.

This role was loosely based on the Ansible roles listed below. I am indebted to
their authors for sharing their work and experience freely.

Using this role you can configure a tailnets with multiple nodes
with each node having its own set of `tailscale up` arguments.
You should set at least that part of the `tailscale` dict somewhere with higher
priority than role defaults, something along these lines:
```
tailscale:
  tailnet:
    - { host: pluto,  args: "--accept-dns=false --accept-routes=true" }
    - { host: saturn, args: "--advertise-exit-node" }
    - { host: casablanca, args: "--accept-dns=false" }
  authkey: "tskey-auth-KEY_IDENTITY-KEY_SECRET"
```


## Use Tailscale as exit node and DNS server for devices

For example, when abroad. The point then is to route *all traffic* via
our Tailscale exit node, *including* DNS queries.

Prerequisite: specify at least one nameserver in the [web-UI DNS settings](https://login.tailscale.com/admin/dns) (I have specified all of my PiHole/unbound servers, for redundancy). To minimise the
risk of DNS leakage, I also enabled the `override local DNS` setting.
But be mindful of the override setting - if the tailnet IP address changes without
you manually updating the nameservers list, you risk breaking DNS (at least you should
notice this when traffic through the exit node stops resolving).
Note that for Pi-Hole to accept traffic from your tailnet nodes you must set it
to **Listen on all interfaces, permit all origins** (default was **Listen only on eth0**).

After setting the `--exit-node` flag on the CLI, don't forget to enable the node
as **exit node** in the web UI as well.

To route all traffic from your Linux computer via the exit node
```
$ sudo tailscale set --exit-node=<IP-exit-node> --accept-dns=true
```

To stop tunneling traffic via the exit node:
```
$ sudo tailscale set --exit-node= --accept-dns=false
```

On Android, simply select the exit node and toggle it on/off.
When you use the exit node feature, DNS traffic is automatically forwarded
(so [no DNS leakage](https://github.com/tailscale/tailscale/issues/1713)).
Awesome!

Tailscale exit nodes can then be shared with other users in our Github org,
or with external users. Very cool!



## Use Tailscale as DNS server for Android devices?

The idea is to *not* route all traffic via the exit node, only the DNS traffic.
This might be useful in certain situations (where you don't mind the ISP seeing
your traffic, but you still want to benefit from our ad/tracker blocking).

I have not tested this properly yet.

+ https://shotor.com/blog/run-your-own-mesh-vpn-and-dns-with-tailscale-and-pihole/
+ https://forum.tailscale.com/t/need-some-help-with-default-dns-when-using-tailscale/341
+ https://github.com/tailscale/tailscale/issues/915
+ https://github.com/tailscale/tailscale/issues/74


## Opt out of client logging

Edit `/etc/default/tailscaled`` and add the following line:
```
TS_NO_LOGS_NO_SUPPORT=true
```

+ https://tailscale.com/kb/1011/log-mesh-traffic


## Notes on running Tailscale client inside LXC container

My DNS server (PiHole + unbound) runs as an LXC container.
In the same container we also run Tailscale.

This works fine. For details on how the LXC profile was setup,
see the [lxd-server role](https://codeberg.org/ansible/lxd-server).



## Tips and tricks

### List currently invoked CLI flags?

Not very obvious, and the output is JSON, but it works:
```
$ tailscale debug prefs
```

+ https://reddit.com/r/Tailscale/comments/rtr8p6/cli_list_current_flags
+ https://github.com/tailscale/tailscale/issues/2130
+ https://forum.tailscale.com/t/how-can-i-find-out-how-tailscale-was-started-on-linux/2287/6



## Links and notes


### Tailscale docs

+ https://tailscale.com/kb/1187/install-ubuntu-2204
+ https://tailscale.com/kb/1241/tailscale-up
+ https://tailscale.com/kb/1072/client-preferences
+ https://tailscale.com/kb/1103/exit-nodes
+ https://tailscale.com/kb/1114/pi-hole
+ https://tailscale.com/kb/1130/lxc-unprivileged
+ https://tailscale.com/kb/1112/userspace-networking
+ https://tailscale.com/kb/1084/sharing/#sharing--exit-nodes
+ https://tailscale.com/kb/1011/log-mesh-traffic


### Blogs, forums, rest of web

+ https://stanislas.blog/2021/08/tailscale


### Ansible roles

+ https://github.com/artis3n/ansible-role-tailscale
+ https://github.com/dockpack/base_tailscale
+ https://github.com/robertdebock/ansible-role-tailscale
+ https://github.com/enzops/ansible-tailscale
+ https://github.com/jason-riddle/ansible-role-tailscale
+ https://github.com/andreygubarev/ansible-tailscale
