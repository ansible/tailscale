# Manual setup of Tailscale on a client

> Written for Ubuntu 22.04 Jammy

For example, during first-time provision of a host behind a firewall that
denies SSH access from the outside world.

Install dependencies:
```{bash}
sudo apt update
sudo apt install gnupg2 gnupg-agent apt-transport-https python3-apt
```

Add Tailscale signing key and apt repo:
```{bash}
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/jammy.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/jammy.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list
```

Install Tailscale client:
```{bash}
sudo apt update
sudo apt install tailscale
```

Connect your machine to your Tailnet and get URL to authenticate:
```{bash}
sudo tailscale up
```

OK!

## Refs

+ https://tailscale.com/download/linux/ubuntu-2204
