# Tailscale causes directly connected routers etc. to fail loading their web admin GUI

> This no longer reflects best practice. Please disregard.

**Scenario**:
Laptop running Ubuntu Server Jammy (with my i3wm configuration per my playbook)
is connected with a normal network cable directly to a router/switch/modem.
The network device is expected to assign an IPv4 address to the laptop (DHCP,
this works whether Tailscale is running or not).

When the laptop user enters the IPv4 address of the network device in the browser,
the device's web-admin GUI is expected to load.

But if Tailscale is running, the page fails to resolve and never loads.
If Tailscale is stopped, the page immediately loads.


## Can we allievate this by adjusting Tailscale settings? Yes.

> Resolved:
> The problem was caused because the laptop was running Tailscale with
> the `exit-node` flag set, which in hindsight, I can understand how it both
> captured the SSH port and DNS lookups.
> From now on, I set `--accept-dns=false` for all nodes unless otherwise required.

Or should I never start Tailscale by default on my laptop (which I often use
to connect to network devices with varying IPv4 address assignments)?


> By default, a Tailscale client will:
> + Allow incoming connections
> + Use Tailscale DNS settings
> https://tailscale.com/kb/1072/client-preferences

```
tailscale up --accept-dns=false
```

> If an Admin has created subnet routes for your tailnet, then Tailscale will
> route your device's traffic for the advertised subnets to the appropriate subnet router.

But note that on Linux, the client **does not use Tailscale subnets by default**.
```
tailscale up --accept-routes=false
```


## Links and notes

+ https://tailscale.com/kb/1072/client-preferences
+ https://tailscale.com/kb/1241/tailscale-up
